package ru.ohotnik.bot.wofh.cache;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

import ru.ohotnik.bot.wofh.driver.HD;
import ru.ohotnik.bot.wofh.entity.BuildEntity;
import ru.ohotnik.bot.wofh.entity.ResoPackEntity;
import ru.ohotnik.bot.wofh.tabs.BuildsTab;

/**
 * Created by ohotnik on 15.02.14.
 */
public class ResourceBuildCache {

    private static Map<Integer, ResoPackEntity> cache = new HashMap<Integer, ResoPackEntity>();

    public ResoPackEntity getResosForBuild(int id) {
        if (!cache.containsKey(id)) {
            addById(id);
        }
        return cache.get(id);
    }

    private void addById(Integer id) {
        WebDriver hd = HD.getInstance();
        BuildEntity entity = BuildsTab.getBuildEntity(id);
        hd.get(entity.getHref());

    }

}
