package ru.ohotnik.bot.wofh.media;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.net.URL;

/**
 * Created by ohotNik on 08.03.14.
 */
public class BuildOk {

    public static void play() {
        try {
            URL url = BuildOk.class.getResource("/media/ok.mp3");

            AudioInputStream ais = AudioSystem.getAudioInputStream(url);

            Clip clip = AudioSystem.getClip();

            clip.open(ais);

            //clip.setFramePosition(0); //устанавливаем указатель на старт
            clip.start(); //Поехали!!!
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
