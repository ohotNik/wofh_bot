package ru.ohotnik.bot.wofh;

import ru.ohotnik.bot.wofh.driver.threads.ThreadFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ohotnik on 22.02.14.
 */
public class CommonWQueue {

  private static Map<Long, QWItem> q = new TreeMap<Long, QWItem>();

  public synchronized QWItem poll() {
    Iterator<Long> i = q.keySet().iterator();
    if (i.hasNext()) {
      long k = i.next();
      QWItem r = q.get(k);
      q.remove(k);
      return r;
    }
    return null;
  }

  public synchronized void addUpgrade(String href) {
    addUpgrade(0, href);
  }

  public synchronized void addAuth() {
    QWItem qwItem = new QWItem(IType.AUTH);
    q.put(0L, qwItem);
    ThreadFactory threadFactory = ThreadFactory.getInstance();
    synchronized (threadFactory) {
      threadFactory.notify();
    }
  }

  public synchronized void addBuildUpdate() {
    QWItem qwItem = new QWItem(IType.BUILDUPDATE);
    q.put(0L, qwItem);
    ThreadFactory threadFactory = ThreadFactory.getInstance();
    synchronized (threadFactory) {
      threadFactory.notify();
    }
  }

  public synchronized void addUpgrade(long time, String href) {
    QWItem qwItem = new QWItem(IType.UPGRADE);
    qwItem.setHref(href);
    qwItem.setTime(time);
    while (q.containsKey(time)) {
      time++;
    }
    q.put(time, qwItem);
    ThreadFactory threadFactory = ThreadFactory.getInstance();
    synchronized (threadFactory) {
      threadFactory.notify();
    }
  }

  public void addCitiesUpdate() {
    QWItem qwItem = new QWItem(IType.CITIESUPDATE);
    q.put(0L, qwItem);
    ThreadFactory threadFactory = ThreadFactory.getInstance();
    synchronized (threadFactory) {
      threadFactory.notifyAll();
    }
  }
}
