package ru.ohotnik.bot.wofh.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by OkhonchenkoAV on 05.02.14.
 */
public class HD {
    @SuppressWarnings({"StaticVariableNamingConvention", "StaticNonFinalField"})
    private static WebDriver driver = null;

  private HD() {
  }

  public static WebDriver getInstance() {
        if (driver == null) {
            //driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_17);
          //noinspection NonThreadSafeLazyInitialization
          driver = new FirefoxDriver();
        }
        return driver;
    }

    public static void quit() {
        if (driver != null) {
            driver.quit();
        }
    }

  public static boolean isActive() {
    return driver!=null;
  }
}
