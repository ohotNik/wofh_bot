package ru.ohotnik.bot.wofh.driver.threads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.ohotnik.bot.wofh.Main;
import ru.ohotnik.bot.wofh.driver.HD;
import ru.ohotnik.bot.wofh.entity.BuildEntity;
import ru.ohotnik.bot.wofh.tabs.BuildsTab;

import java.util.List;

/**
 * Created by ohotnik on 08.02.14.
 */
public class CityParseThread extends Thread {

    @Override
    public void run() {

        Main.clearBuilds();
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        WebDriver webDriver = HD.getInstance();
        webDriver.findElement(By.id("m2_town")).click();
        WebElement map = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("Map")));
        List<WebElement> list = map.findElements(By.tagName("area"));
        for (WebElement e : list) {
            String title = e.getAttribute("title");
            String href = e.getAttribute("href");
            if (href == null || href.contains("#")) {
                continue;
            }
            BuildEntity buildEntity = new BuildEntity();
            buildEntity.setName(title);
            buildEntity.setHref(href);
            buildEntity.setId(Integer.parseInt(href.split("=")[1]));
            BuildsTab.add(href, buildEntity);
        }
        Main.repaintBuilds();
        System.out.println("Считаны поля");

    }
}
