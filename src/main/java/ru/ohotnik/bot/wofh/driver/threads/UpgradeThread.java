package ru.ohotnik.bot.wofh.driver.threads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.ohotnik.bot.wofh.CommonWQueue;
import ru.ohotnik.bot.wofh.Main;
import ru.ohotnik.bot.wofh.driver.HD;

import java.util.Date;
import java.util.List;

/**
 * Created by ohotnik on 16.02.14.
 */
public class UpgradeThread extends Thread {

  private String href;

  public void setHref(String href) {
    this.href = href;
  }

  @Override
  public void run() {
    WebDriver webDriver = HD.getInstance();
    synchronized (webDriver) {
      webDriver.get(href);
      WebElement upgrade = (new WebDriverWait(webDriver, 10))
          .until(ExpectedConditions.presenceOfElementLocated(By.className("bbtn1")));
      if ("Нельзя улучшить, это максимальный уровень здания".equals(upgrade.findElement(By.tagName("img")).getAttribute("title"))) {
        Main.log("Для " + href + " достигнут максимальный уровень. Исключено из очереди.");
      } else if ("Очередь строительства заполнена".equals(upgrade.findElement(By.tagName("img")).getAttribute("title"))) {
        //Main.log("Очередь строительства заполнена");
        CommonWQueue commonWQueue = new CommonWQueue();
        commonWQueue.addUpgrade(href);
      } else if ("Недостаточно ресурсов для улучшения".equals(upgrade.findElement(By.tagName("img")).getAttribute("title"))) {
        CommonWQueue commonWQueue = new CommonWQueue();
        commonWQueue.addUpgrade(getTime(webDriver) + System.currentTimeMillis(), href);
      } else {
        upgrade.click();
        Main.log("Апгрейд: " + href);
      }
      upgrade = (new WebDriverWait(webDriver, 10))
          .until(ExpectedConditions.presenceOfElementLocated(By.id("m2_town")));
      webDriver.get(upgrade.getAttribute("href"));
    }

    CityParseThread cityParseThread = new CityParseThread();
    cityParseThread.run();
  }

  private long getTime(WebDriver driver) {
    WebElement element = driver.findElement(By.className("build_title"));
    List<WebElement> spans = element.findElements(By.tagName("span"));
    for (WebElement span : spans) {
      if (!"Достаточно ресурса".equals(span.getAttribute("title"))) {
        String t = span.getAttribute("title");
        t = t.substring(t.lastIndexOf(' ') + 1);
        String[] time = t.split(":");
        if (time.length == 3) {
          return (getInt(time[2]) * 1000 + getInt(time[1]) * 60000 + getInt(time[0]) * 3600000);
        } else if (time.length == 2) {
          return (getInt(time[1]) * 1000 + getInt(time[0]) * 60000);
        } else if (time.length == 1) {
          return (getInt(time[0]) * 1000);
        }
      }
    }
    return 0;
  }

  private int getInt(String s) {
    return Integer.parseInt(s);
  }

}
