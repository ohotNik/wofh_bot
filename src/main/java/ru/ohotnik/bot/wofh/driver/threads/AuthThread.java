package ru.ohotnik.bot.wofh.driver.threads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ru.ohotnik.bot.wofh.Main;
import ru.ohotnik.bot.wofh.driver.HD;
import ru.ohotnik.bot.wofh.tabs.FormConnectorUtils;
import ru.ohotnik.bot.wofh.tabs.LogTab;

/**
 * Created by ohotnik on 08.02.14.
 */
public class AuthThread extends Thread {

    @Override
    public void run() {
        WebDriver hd = HD.getInstance();
        hd.get("http://www.wofh.ru");
        WebElement element = hd.findElement(By.name("login"));
        element.sendKeys(FormConnectorUtils.getLogin());
        element = hd.findElement(By.name("password"));
        element.sendKeys(FormConnectorUtils.getPassword());
        element = hd.findElement(By.xpath("//input[@class='btn_mlog'][@value='Вход']"));
        element.click();
        System.out.println("Вошли на сайт");
        Main.log("Авторизовались");

    }

}
