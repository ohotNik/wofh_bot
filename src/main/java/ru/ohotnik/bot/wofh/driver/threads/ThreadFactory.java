package ru.ohotnik.bot.wofh.driver.threads;

import ru.ohotnik.bot.wofh.CommonWQueue;
import ru.ohotnik.bot.wofh.IType;
import ru.ohotnik.bot.wofh.QWItem;

/**
 * Created by ohotnik on 23.02.14.
 */
public class ThreadFactory extends Thread {

  private static ThreadFactory instance;

  public static ThreadFactory getInstance() {
    if (instance == null) {
      instance = new ThreadFactory();
      instance.start();
    }
    return instance;
  }

  @Override
  public void run() {
    CommonWQueue commonWQueue = new CommonWQueue();
    QWItem item;
    try {
      while (true) {
        while ((item = commonWQueue.poll()) != null) {
          if (IType.UPGRADE == item.getType()) {
            if (System.currentTimeMillis() > item.getTime()) {
              UpgradeThread upgradeThread = new UpgradeThread();
              upgradeThread.setHref(item.getHref());
              upgradeThread.run();
            } else {
              commonWQueue.addUpgrade(item.getTime(), item.getHref());
            }
          }
          if (IType.AUTH == item.getType()) {
            AuthThread authThread = new AuthThread();
            authThread.run();
            continue;
          }
          if (IType.BUILDUPDATE == item.getType()) {
            CityParseThread cityParseThread = new CityParseThread();
            cityParseThread.run();
          }
          sleep(60000);
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
