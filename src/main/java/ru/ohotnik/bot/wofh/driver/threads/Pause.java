package ru.ohotnik.bot.wofh.driver.threads;

import ru.ohotnik.bot.wofh.Alert;

/**
 * Created by ohotnik on 08.02.14.
 */
public class Pause extends Thread {

    public static void pause(int seconds) {
        try {
            Pause.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            Alert.show("Произошла ошибка: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
