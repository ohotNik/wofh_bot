package ru.ohotnik.bot.wofh.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ohotnik on 15.02.14.
 */
public class ResoPackEntity {

    private Map<String, Integer> pack = new HashMap<String, Integer>();

    public boolean contains(String reso) {
        return pack.containsKey(reso);
    }

    public Map<String, Integer> getPack() {
        // todo переделать на копирование/  клонирование
        return pack;
    }

    public void put(String title, Integer value) {
        pack.put(title, value);
    }

}
