package ru.ohotnik.bot.wofh.entity;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import ru.ohotnik.bot.wofh.CommonWQueue;
import ru.ohotnik.bot.wofh.cache.ResourceBuildCache;
import ru.ohotnik.bot.wofh.tabs.BuildsTab;

/**
 * Created by ohotnik on 15.02.14.
 */
public class BuildContextMenu extends ContextMenu {

	ResourceBuildCache cache = new ResourceBuildCache();

	public BuildContextMenu(int id, boolean newSpace) {
		super();
		if (newSpace) {
			MenuItem buildPart = new MenuItem("Строить");
			this.getItems().add(buildPart);
		}
		if (!newSpace) {
			MenuItem upgrade = new MenuItem("Апгрейд");
			upgrade.setOnAction(createHandler(id));
			this.getItems().add(upgrade);
		}
	}

	private EventHandler<ActionEvent> createHandler(final int id) {
		EventHandler<ActionEvent> handler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				BuildEntity be = BuildsTab.getBuildEntity(id);
				CommonWQueue commonWQueue = new CommonWQueue();
				commonWQueue.addUpgrade(be.getHref());
//                UpgradeThread upgradeThread = new UpgradeThread();
//                upgradeThread.setHref(be.getHref());
//                upgradeThread.start();
			}
		};
		return handler;
	}

}
