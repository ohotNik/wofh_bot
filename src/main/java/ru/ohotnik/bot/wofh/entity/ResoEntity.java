package ru.ohotnik.bot.wofh.entity;

/**
 * Created by ohotnik on 15.02.14.
 */
public class ResoEntity {

    private String title;
    private int value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
