package ru.ohotnik.bot.wofh;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import ru.ohotnik.bot.wofh.driver.HD;
import ru.ohotnik.bot.wofh.tabs.Auth;
import ru.ohotnik.bot.wofh.tabs.BuildsTab;
import ru.ohotnik.bot.wofh.tabs.CitiesTab;
import ru.ohotnik.bot.wofh.tabs.LogTab;

/**
 * Created by OkhonchenkoAV on 05.02.14.
 */
public class Main extends Application {

  public static void main(String[] args) {
    Log.message("Старт");
    launch(args);
  }

  public static void clearBuilds() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        synchronized (this) {
          BuildsTab.clear();
          notifyAll();
        }
      }
    });
  }

  public static void log(final String message) {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        LogTab.log(message);
      }
    });
  }

  public static void repaintBuilds() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        synchronized (this) {
          BuildsTab.repaint();
        }
      }
    });
  }

  @Override
  public void stop() throws Exception {
    super.stop();
    HD.quit();
  }

  @Override
  public void start(Stage stage) throws Exception {
    //new BuildOk().play();
    stage.setTitle("wofh 1.0 - SNAPSHOT");
    TabPane root = new TabPane();
    addTabs(root);
    stage.setScene(new Scene(root, 400, 300));
    stage.show();
    LogTab.log("Запуск");
  }

  private static void addTabs(TabPane pane) {
    pane.getTabs().add(new Auth());
    //pane.getTabs().add(new Net());
    pane.getTabs().add(new BuildsTab());
    pane.getTabs().add(new LogTab());
    pane.getTabs().add(new CitiesTab());
  }
}
