package ru.ohotnik.bot.wofh.tabs;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import ru.ohotnik.bot.wofh.driver.HD;

import java.util.Iterator;

/**
 * Created by ohotNik
 * Date : 20.04.14
 * Time : 9:58
 * Description :
 */
public class CitiesTab extends Tab {

  public static final String TAB_NAME = "Города";
  public static final String TH_CITY = "Город";
  private static final GridPane GRID_PANE = new GridPane();
  public static final String TH_POP = "Население";
  public static final String TH_BUILD = "Стройка";
  public static final String TH_ARMY = "Призыв";
  public static final double GAP_VALUE = 15.0;

  public CitiesTab() {
    setText(TAB_NAME);
    setClosable(false);
    setContent(GRID_PANE);
    GRID_PANE.setHgap(GAP_VALUE);
    GRID_PANE.setVgap(GAP_VALUE);
    updateTab();
  }

  public static void updateTab() {
    cleanTab(GRID_PANE);
    GRID_PANE.add(new Label(TH_CITY), 0, 0);
    GRID_PANE.add(new Label(TH_POP), 1, 0);
    GRID_PANE.add(new Label(TH_BUILD), 2, 0);
    GRID_PANE.add(new Label(TH_ARMY), 3, 0);
    if (HD.isActive()) {
    }
  }

  private static void cleanTab(Pane pane) {
    Iterator<Node> iterator = pane.getChildren().iterator();
    while (iterator.hasNext()) {
      iterator.next();
      iterator.remove();
    }
  }

}
