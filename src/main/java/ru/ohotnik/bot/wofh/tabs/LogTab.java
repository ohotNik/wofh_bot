package ru.ohotnik.bot.wofh.tabs;

import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;

import java.util.Date;

/**
 * Created by ohotNik on 08.03.14.
 */
public class LogTab extends Tab {

    private static TextArea ta = new TextArea();

    public LogTab() {
        super();
        setText("Лог");
        setClosable(false);
        setContent(ta);
    }

    public static void log(String message) {
        StringBuilder sb = new StringBuilder();
        Date now = new Date();
        sb.append(now);
        sb.append(" : ");
        sb.append(message);
        sb.append("\n");
        ta.appendText(sb.toString());
    }

}
