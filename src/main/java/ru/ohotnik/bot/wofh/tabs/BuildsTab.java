package ru.ohotnik.bot.wofh.tabs;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import ru.ohotnik.bot.wofh.entity.BuildContextMenu;
import ru.ohotnik.bot.wofh.entity.BuildEntity;
import ru.ohotnik.bot.wofh.tabs.listeners.AutoBuildAction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ohotnik on 08.02.14.
 */
public class BuildsTab extends Tab {

	private static Map<String, BuildEntity> map = new HashMap<String, BuildEntity>();
	private static GridPane root = new GridPane();
	private static BuildsTab instance;

	public static void clear() {
		root = new GridPane();
		map.clear();
	}

	public static BuildEntity getBuildEntity(int id) {
		for (BuildEntity e : map.values()) {
			if (id == e.getId()) {
				return e;
			}
		}
		return null;
	}

	public static void add(String name, BuildEntity buildEntity) {
		map.put(name, buildEntity);
	}

	public BuildsTab() {
		super();
		this.setText("Постройки");
		this.setClosable(false);
		this.setContent(root);
		instance = this;
		repaint();
	}

	public static void repaint() {
		Button button = new Button("Автостройка");
		button.setOnAction(new AutoBuildAction());
		root.add(button, 0, 0);

		root.add(new Label("id"), 0, 1);
		root.add(new Label("Постройка"), 1, 1);
		int row = 2;
		for (BuildEntity e : map.values()) {
			Label lbl = new Label("" + e.getName());
			boolean flag = checkNew(e.getName());
			lbl.setContextMenu(new BuildContextMenu(e.getId(), flag));
			root.add(new Label("" + e.getId()), 0, row);
			root.add(lbl, 1, row);
			row++;
		}
		instance.setContent(root);
	}

	private static boolean checkNew(String name) {
		return "Равнина".equals(name) || "Берег".equals(name) || "Центр города".equals(name) ||
				"Периметр".equals(name) || "Холм".equals(name) || "Чудо Света".equals(name);
	}

}
