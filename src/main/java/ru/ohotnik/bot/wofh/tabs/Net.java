package ru.ohotnik.bot.wofh.tabs;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import ru.ohotnik.bot.wofh.tabs.listeners.IsProxyListener;

/**
 * Created by OkhonchenkoAV on 05.02.14.
 */
public class Net extends Tab {

    private static GridPane content = new GridPane();

    private static Map<COMPONENTS, Node> components = new HashMap<COMPONENTS, Node>();

    public Net() {
        super();
        this.setText("Сеть");
        this.setClosable(false);
        configComponents();
        configListeners();
        configPane();
    }

    private void configComponents() {
        TextField host = new TextField();
        host.setDisable(true);
        components.put(COMPONENTS.HOST, host);
        TextField port = new TextField();
        port.setDisable(true);
        components.put(COMPONENTS.PORT, port);
        CheckBox checkBox = new CheckBox();
        checkBox.setText("вкл");
        components.put(COMPONENTS.USE_PROXY, checkBox);
    }

    private void configListeners() {
        CheckBox cb = (CheckBox) components.get(COMPONENTS.USE_PROXY);
        cb.selectedProperty().addListener(new IsProxyListener());
    }

    private void configPane() {
        content.setHgap(15);
        content.setVgap(10);
        content.setAlignment(Pos.CENTER);
        content.add(new Label("Прокси:"), 0, 0);
        content.add(components.get(COMPONENTS.USE_PROXY), 1, 1);
        content.add(new Label("Host:"), 0, 2);
        content.add(components.get(COMPONENTS.HOST), 1, 2);
        content.add(new Label("Post:"), 0, 3);
        content.add(components.get(COMPONENTS.PORT), 1, 3);

        this.setContent(content);
    }

    private static enum COMPONENTS {
        HOST,
        PORT,
        USE_PROXY
    }

    protected static boolean isProxyEnabled() {
        return ((CheckBox) components.get(COMPONENTS.USE_PROXY)).isSelected();
    }

    protected static void setProxy(boolean b) {
        components.get(COMPONENTS.HOST).setDisable(!b);
        components.get(COMPONENTS.PORT).setDisable(!b);
    }

  protected static String getProxyHost() {
    return ((TextField) components.get(COMPONENTS.HOST)).getText();
  }

  protected static String getProxyPort() {
    return ((TextField) components.get(COMPONENTS.PORT)).getText();
  }

}
