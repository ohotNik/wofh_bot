package ru.ohotnik.bot.wofh.tabs;

/**
 * Created by ohotnik on 05.02.14.
 */
public class FormConnectorUtils {

    public static void netProxySelection(boolean b) {
        Net.setProxy(b);
    }

  public static boolean useProxy() {
    return Net.isProxyEnabled();
  }

  public static String getProxyHost() {
    return Net.getProxyHost();
  }

  public static String getProxyPort() {
    return Net.getProxyPort();
  }

    public static String getLogin() {
        return Auth.getLogin();
    }

    public static String getPassword() {
        return Auth.getPassword();
    }

    public static boolean isSaveAuth() {
        return Auth.isSave();
    }

}
