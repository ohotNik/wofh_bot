package ru.ohotnik.bot.wofh.tabs.listeners;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javafx.event.Event;
import javafx.event.EventHandler;
import ru.ohotnik.bot.wofh.Alert;
import ru.ohotnik.bot.wofh.CommonWQueue;
import ru.ohotnik.bot.wofh.driver.threads.MasterThread;
import ru.ohotnik.bot.wofh.tabs.FormConnectorUtils;

/**
 * Created by ohotnik on 08.02.14.
 */
public class EnterButtonListener implements EventHandler {

    String login;
    String password;

    @Override
    public void handle(Event event) {
        login = FormConnectorUtils.getLogin();
        password = FormConnectorUtils.getPassword();
        if ("".equals(login)) {
            Alert.show("Укажите логин!");
            return;
        }
        if ("".equals(password)) {
            Alert.show("Укажите пароль!");
            return;
        }
        if (FormConnectorUtils.isSaveAuth()) {
            createProps();
        }
        CommonWQueue commonWQueue = new CommonWQueue();
        commonWQueue.addAuth();
        MasterThread masterThread = new MasterThread();
        masterThread.start();
    }

    private void createProps() {
        Properties prop = new Properties();
        OutputStream output = null;

        try {

            output = new FileOutputStream("config.properties");

            prop.setProperty("login", login);
            prop.setProperty("password", password);

            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
