package ru.ohotnik.bot.wofh.tabs.listeners;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import ru.ohotnik.bot.wofh.tabs.FormConnectorUtils;

/**
 * Created by ohotnik on 05.02.14.
 */
public class IsProxyListener implements ChangeListener<Boolean> {

    @Override
    public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean aBoolean2) {
        FormConnectorUtils.netProxySelection(aBoolean2);
    }
}
