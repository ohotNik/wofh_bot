package ru.ohotnik.bot.wofh.tabs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import ru.ohotnik.bot.wofh.tabs.listeners.EnterButtonListener;

/**
 * Created by OkhonchenkoAV on 05.02.14.
 */
public class Auth extends Tab {

    private static GridPane content = new GridPane();

    private static Map<COMPONENTS, Node> components = new HashMap<COMPONENTS, Node>();

    public Auth() {
        super();
        this.setText("Вход");
        this.setClosable(false);
        configComponents();
        configPane();
    }

    private void configComponents() {
        TextField login = new TextField();
        PasswordField passwordField = new PasswordField();
        CheckBox saveMe = new CheckBox();
        saveMe.setText("Запомнить");
        Button button = new Button("Вход");
        button.setOnAction(new EnterButtonListener());

        List<String> props = readProps();
        if (!props.isEmpty()) {
            login.setText(props.get(0));
            passwordField.setText(props.get(1));
        }

        components.put(COMPONENTS.LOGIN, login);
        components.put(COMPONENTS.PASSWORD, passwordField);
        components.put(COMPONENTS.ENTER_BUTTON, button);
        components.put(COMPONENTS.SAVE_ME, saveMe);
    }

    private void configPane() {
        content.setHgap(15);
        content.setVgap(10);
        content.setAlignment(Pos.CENTER);
        content.add(new Label("Аккаунт:"), 0, 0);
        content.add(new Label("Логин:"), 0, 1);
        content.add(components.get(COMPONENTS.LOGIN), 1, 1);
        content.add(new Label("Пароль:"), 0, 2);
        content.add(components.get(COMPONENTS.PASSWORD), 1, 2);
        content.add(components.get(COMPONENTS.SAVE_ME), 1, 3);
        content.add(components.get(COMPONENTS.ENTER_BUTTON), 1, 4);

        this.setContent(content);
    }

    private static enum COMPONENTS {
        LOGIN,
        PASSWORD,
        ENTER_BUTTON,
        SAVE_ME
    }

    protected static String getLogin() {
        return ((TextField) components.get(COMPONENTS.LOGIN)).getText();
    }

    protected static String getPassword() {
        return ((PasswordField) components.get(COMPONENTS.PASSWORD)).getText();
    }

    protected static boolean isSave() {
        return ((CheckBox) components.get(COMPONENTS.SAVE_ME)).isSelected();
    }

    private List<String> readProps() {

        File f = new File("config.properties");
        List<String> result = new ArrayList<String>();
        if (f.exists()) {
            Properties prop = new Properties();
            InputStream input = null;

            try {

                input = new FileInputStream(f);

                // load a properties file
                prop.load(input);

                // get the property value and print it out
                result.add(prop.getProperty("login"));
                result.add(prop.getProperty("password"));

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result;
    }

}
