package ru.ohotnik.bot.wofh;

/**
 * Created by ohotnik on 23.02.14.
 */
public class QWItem {

  private IType type;
  private String href;
  private long time;

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public QWItem(IType t) {
    if (IType.UPGRADE == t) {
      type = t;
      return;
    }
    if (IType.AUTH == t) {
      type = t;
      return;
    }
    if (IType.BUILDUPDATE == t) {
      type = t;
      return;
    }
    throw new RuntimeException("Неизвестный тип работы!!!");
  }

  public IType getType() {
    return type;
  }

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }
}
